#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
# To be able to import headers on the level above
sys.path.append(os.path.abspath('..'))  # Comment this line to be able to use pipreqs

import unittest

import httpretty
import sure

import common.utils
from config import Config
from bitbucket import Bitbucket


PROGRAM_PATH = os.path.dirname(os.path.abspath(__file__))
MOCK_DIR = os.path.join(PROGRAM_PATH, 'mock')


class TestUtils(unittest.TestCase):
    def test_build_header(self):
        header = common.utils.build_header('some_product', 100)
        header.should.equal(
            '*********************\n'
            '* some_product v100 *\n'
            '*********************\n'
        )

    def test_get_file_content(self):
        content = common.utils.get_file_content(os.path.join(MOCK_DIR, 'data.txt'))
        content.should.equal('some data\nsome other data')


class TestConfig(unittest.TestCase):
    def test_config_loading(self):
        config = Config()
        config.load(os.path.join(MOCK_DIR, 'config.ini'))

        config.check_interval_secs.should.equal(300.0)  # TODO: check float comparison made by sure library

        config.bitbucket_username.should.equal('some_username')
        config.bitbucket_password.should.equal('some_password')

        config.slack_api_token.should.equal('xoxp-2894084035-2894084043-2894574745-ff0c99')
        config.slack_channel.should.equal('dev')
        config.slack_username.should.equal('bitbucketeye')
        config.slack_icon_url.should.equal('http://chrisbeaver.com/wp-content/uploads/2014/07/bitbucket-logo.png')


class TestBitbucketCommunication(unittest.TestCase):
    def setUp(self):
        self.bitbucket = Bitbucket(username='dummy', password='dummy')

    @httpretty.activate
    def test_get_repos(self):
        httpretty.register_uri(httpretty.GET, 'https://api.bitbucket.org/1.0/user/repositories',
                               body=common.utils.get_file_content(os.path.join(MOCK_DIR, 'repos.json')))

        repos = self.bitbucket.get_repos()
        repos[0]['name'].should.equal('lorify')
        repos[0]['last_updated'].should.equal('2016-03-09T16:18:49.701')
        repos[1]['name'].should.equal('lorify-ff')
        repos[1]['last_updated'].should.equal('2016-03-09T16:19:23.445')
        repos[2]['name'].should.equal('lorify-userscript')
        repos[2]['last_updated'].should.equal('2016-03-10T09:40:56.067')


if __name__ == '__main__':
    unittest.main()
