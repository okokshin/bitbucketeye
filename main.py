#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
import traceback

from bitbucket import Bitbucket
from common.simple_logger import SimpleLogger
import common.utils
from config import Config

from dateutil.parser import parse
from peewee import *
import slack
import slack.chat

# To disable the following warning:
# InsecurePlatformWarning: A true SSLContext object is not available.
# This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail.
# For more information, see https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()


###########################
# Constants
###########################

PRODUCT_NAME = 'bitbucketeye'
PRODUCT_VERSION = 300
PROGRAM_PATH = os.path.dirname(os.path.abspath(__file__))
CONFIG_FILE_NAME = os.path.join(PROGRAM_PATH, 'config.ini')
LOGS_DIR = os.path.join(PROGRAM_PATH, 'logs')


###########################
# Global variables
###########################

logger = None
repos_db = SqliteDatabase('repos.db')


###########################
# Classes
###########################

class RepoLastUpdate(Model):
    name = TextField(unique=True)
    last_update = TextField()

    class Meta:
        # peewee creates table names without 's' postfix, so let's do it manually
        db_table = 'reposlastupdates'
        database = repos_db


###########################
# Methods
###########################

def panic(msg):
    logger.critical(msg)
    sys.exit(1)


def init_logger():
    global logger

    logger = SimpleLogger(LOGS_DIR)


def print_header():
    header = common.utils.build_header(PRODUCT_NAME, PRODUCT_VERSION)
    for line in header.split('\n'):
        logger.info(line)


def load_config():
    global config

    config = Config()
    config.load(CONFIG_FILE_NAME)


def connect_to_db():
    repos_db.connect()


def send_to_slack(msg):
    slack.chat.post_message(
        channel=config.slack_channel,
        text=msg,
        username=config.slack_username,
        icon_url=config.slack_icon_url
    )


def track_events(bitbucket, owner, repo, last_updated, start=0):
    logger.info('Tracing new events in "' + owner + '/' + repo + '"...')
    events = bitbucket.get_events(owner, repo, start)

    if len(events) == 0:
        return

    last_event_created = last_updated

    for event in events:
        event_created = parse(event['utc_created_on'])

        if event_created > last_updated:
            event_type = event['event']
            event_author = event['user']['display_name']

            if event_type == 'create':
                send_to_slack(event_author + ' created "' + repo + '"')

            if event_type == 'pushed':
                send_to_slack(event_author + ' pushed a changeset to "' + repo + '"')

            if event_type == 'commit':
                send_to_slack(event_author + ' push "' + event['node'] + '" in "' + repo + '"')

            if event_type == 'cset_like':
                send_to_slack(event_author + ' approved "' + event['node'] + '" in "' + repo + '"')

            if event_type == 'pullrequest_like':
                send_to_slack(event_author + ' approved pull-request in "' + repo + '"')

            last_event_created = event_created
        else:
            return

    track_events(bitbucket, owner, repo, last_event_created, start + 50)


def main():
    init_logger()

    print_header()

    logger.info('Loading config...')
    load_config()

    logger.info('Connecting to DB...')
    connect_to_db()

    first_run = False
    if not RepoLastUpdate.table_exists():
        first_run = True
        logger.info('Creating "{}" table...'.format(RepoLastUpdate._meta.db_table))
        repos_db.create_table(RepoLastUpdate)

    slack.api_token = config.slack_api_token

    while True:
        logger.info('Checking updates...')

        bitbucket = Bitbucket(config.bitbucket_username, config.bitbucket_password)
        repos = bitbucket.get_repos()
        for repo in repos:
            name = repo['slug']
            owner = repo['owner']
            last_updated = parse(repo['utc_last_updated'])

            try:
                repo_db_record = RepoLastUpdate.get(RepoLastUpdate.name == name)
                if last_updated > parse(repo_db_record.last_update):
                    if not first_run:
                        track_events(bitbucket, owner, name, parse(repo_db_record.last_update))
                    repo_db_record.last_update = repo['utc_last_updated']
                    repo_db_record.save()
            except DoesNotExist:
                if not first_run:
                    track_events(bitbucket, owner, name, parse(repo_db_record.last_update))
                repo_db_record = RepoLastUpdate(name=name, last_update=repo['utc_last_updated'])
                repo_db_record.save()

        logger.info('Waiting {} secs...'.format(config.check_interval_secs))
        time.sleep(config.check_interval_secs)


if __name__ == '__main__':
    try:
        main()
    except:
        panic('ERROR. Traceback:\n{0}'.format(traceback.format_exc()))
    finally:
        common.utils.show_message_box(
            text='Program was terminated. \nSee log for details',
            caption=PRODUCT_NAME
        )
