import ConfigParser


class Config:
    def __init__(self):
        pass

    def load(self, filename):
        config = ConfigParser.SafeConfigParser()
        config.read(filename)

        self.check_interval_secs = float(config.get('general', 'check_interval_secs'))

        self.bitbucket_username = config.get('bitbucket', 'username')
        self.bitbucket_password = config.get('bitbucket', 'password')

        self.slack_api_token = config.get('slack', 'api_token')
        self.slack_channel = config.get('slack', 'channel')
        self.slack_username = config.get('slack', 'username')
        self.slack_icon_url = config.get('slack', 'icon_url')
